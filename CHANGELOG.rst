^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
Changelog for package ros_peerjs
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

0.1.8 (2017-05-05)
------------------
* downgrade ws version to match xenial nodejs version
* add executable permissions
* Contributors: Gérald Lelong

0.1.7 (2017-04-25)
------------------
* add git as build dependency
* Contributors: Gérald Lelong

0.1.6 (2017-04-25)
------------------
* remove node_modules
* Contributors: Gérald Lelong

0.1.5 (2017-04-25)
------------------
* still trying to rebuild node packages
* Contributors: Gérald Lelong

0.1.4 (2017-04-25)
------------------
* npm rebuild
* Contributors: Gérald Lelong

0.1.3 (2017-04-25)
------------------
* install js nodes to share directory to pass build
* Contributors: Gérald Lelong

0.1.2 (2017-04-24)
------------------
* add node_modules
* Contributors: Gérald Lelong

0.1.1 (2017-04-20)
------------------
* add dependencies
* install with cmake
* test page and script
* small edit
* fix ws install
* gitignore + name on CMakeLists.txt
* first
* Contributors: Gérald Lelong, nmartignoni
